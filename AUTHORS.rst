Authors
=======

The main developers of the energy related operators are

* Adel Kara Slimane (CEA Saclay)
* Phillipp Reck (Previously at CEA Saclay)
* Geneviève Fleury (CEA Saclay)

Contributors include

* Thomas Kloss (HLRS, CEA Grenoble)
* Christoph Groth (CEA Grenoble)
* Xavier Waintal (CEA Grenoble)

(CEA = Commissariat à l'énergie atomique et aux énergies alternatives,
HLRS = Höchstleistungsrechenzentrum Stuttgart)
