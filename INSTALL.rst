.. _install:

Install and run
===============

The requirements come from kwant and tkwant. `Install instructions <https://kwant-project.org/doc/1/pre/install>`_ from Kwant have been used to write a more thorough guide in what follows.

Packages for Ubuntu (≥ 18.04)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Enable the “universe” and “multiverse” repositories: open an
   application called “Software & updates”. In “Ubuntu Software” tab,
   check the the repos corresponding to “universe” and “multiverse”.

2. Install some additional packages
    .. code:: shell

        $ sudo apt install git cython3 libhwloc-dev libibverbs-dev libblas-dev libnuma-dev gfortran libmumps-scotch-dev build-essential libopenblas-dev liblapack-dev librsb-dev

3. Install python dependencies
    .. code:: shell
   
        $ sudo apt install python3-pip ipython3 python3-mpi4py python3-dill python3-scipy python3-dev python3-matplotlib python3-pytest python3-sympy python3-simplegeneric python3-setuptools-scm

4. Install python virtualenv to be able to have python virtual environments:
    .. code:: shell
        
        $ sudo apt install virtualenv
        


Packages for Fedora
~~~~~~~~~~~~~~~~~~~

The same tools and libraries need to be installed, but they have
different names.

1. Additionnal packages
    .. code:: shell
        
        $ sudo dnf install scotch scotch-devel MUMPS-openmpi-devel MUMPS-openmpi MUMPS MUMPS-devel MUMPS-mpich mpich mpich-devel metis metis-devel openblas* metis64 metis64-devel git gcc gcc-c++ gcc-gfortran lapack-static lapack-devel lapack64-static lapack64-devel``

2. Python packages
    .. code:: shell
        
        $ sudo dnf install python3-pip python3-dill python3-Cython python3-devel python3-scipy python3-matplotlib python3-pytest python3-sympy python3-mpmath python3-pickleshare python3-pygments python3-pyparsing python3-simplegeneric python3-traitlets python3-configparser python3-mpi4py-mpich python3-mpi4py-openmpi python3-setuptools_scm

3. Install python virtualenv to be able to have python virtual environments:
    .. code:: shell
        
        $ sudo dnf install python3-virtualenv python3-virtualenv-api python3-virtualenv-clone python3-virtualenvwrapper python3-pytest-virtualenv

Install instructions for Linux systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. **Optional: Use a virtual environment** Create a folder that will
   contain all the virtual environment folders, then create a virtual
   environment in it (a folder that will contain all the pip3 installs)
   and finally activate it (it will change the paths so that pip3 will
   install packages in it and you use its python3 executable)

    .. code:: shell
        
        $ mkdir envs
        $ cd envs
        $ virtualenv --system-site-packages tkwantoperator
        $ source tkwantoperator/bin/activate

   *Note*: the last line “activates” the virtual environment. All pip
   installs will be made in it. Deactivating the virtual environment
   simply amounts to write
   
    .. code:: shell
        
        $ deactivate
        
   Removing a virtual environment simply amounts to deleting its root folder,
   in our case by deleting the folder ``envs/tkwantoperator``.

2. **Fedora specific:** Load openmpi environment
    .. code:: shell
        
        $ module load mpi/openmpi-x86_64 # can be mpi/mpich-x86_64
        $ export CPATH=/usr/include/openmpi-x86_64/ # can be /usr/include/mpich-x86_64/, it should be consistent with the above line

3. Install latest tkwant along with its dependencies, then tkwantoperator:
    .. code:: shell
        
        $ pip3 install git+https://gitlab.kwant-project.org/kwant/tkwant.git
        $ pip3 install git+https://gitlab.kwant-project.org/kwant/tkwantoperator.git

   *Note*: 

   - If ``kwant`` fails to install, cloning it first and installing the pulled code may work as it is more recent
        .. code:: shell

                $ source tkwantoperator/bin/activate
                $ git clone https://gitlab.kwant-project.org/kwant/kwant.git
                $ cd kwant
                $ pip3 install .

   - To install updates in any of the git repositories, simply redo the pip install.
   - Install may fail on linux systems with the latest python (3.9+) and pip version. If so, please retry with the extra ``--no-build-isolation`` argument to pip



Running scripts
~~~~~~~~~~~~~~~

You should now be able to use kwant, tkwant and tkwantoperator in the command line in
that virtual environment (if a virtual environment has been used). For example one can run the example script ``quantum_dot.py``
from :ref:`tutorial` :

.. code:: shell

    $ wget https://gitlab.kwant-project.org/kwant/tkwantoperator/-/raw/master/doc/source/tutorial/quantum_dot.py
    $ python3 quantum_dot.py


.. _parallel-calculations:

Parallel calculations
^^^^^^^^^^^^^^^^^^^^^

To make the simulation faster, it is possible to run scripts on several
parallel cores. For example, one can run ``quantum_dot.py`` on 4 cores 
with the following command:

.. code:: shell

   $ mpirun -n 4 quantum_dot.py

Note that in the above command, ``mpirun`` will always use the **first** 
four cores of the CPU: if a second script is run in parallel using the same 
command, the same cores will be used to run it, which will make the simulation 
slower, since the same cores are used for both simulations. One way around that
is to use ``mpirun`` with ``--bind-to none`` option:

.. code:: shell

   $ mpirun -n 4 --bind-to none python3 quantum_dot.py

The ``-bind-to none`` option is there to not force the OS to use specific 
cores to run the 4 copies. This option comes with a caveat: for CPUs with 
`SMT <https://en.wikipedia.org/wiki/Simultaneous_multithreading>`_ enabled
the OS may use the additional hardware threads as cores to fill out the asked number 
of cores to use by the ``-n`` option, it may also move them around between 
hardware threads, which can decrease the performance compared
to ``-n`` option used alone. 

There’s also the possibility to
chose the hardware threads (whether `SMT <https://en.wikipedia.org/wiki/Simultaneous_multithreading>`_ 
is enabled or not) on which to run the script

.. code:: shell

   $ mpirun -n 5 --cpu-set 0-4 --bind-to core python3 quantum_dot.py

Where threads 0,1,2,3,4 will be used for the calculations, the same
command can be written:

.. code:: shell

   $ mpirun -n 5 --cpu-set 0,1,2,3,4 --bind-to core python3 quantum_dot.py

To run two scripts on the same machine, both on several cores, the best is to manually chose different cores for each script
through the ``--cpu-set`` option.

**N.B.:** If the used computer has 20 cores, 40 threads (when `SMT <https://en.wikipedia.org/wiki/Simultaneous_multithreading>`_
is enabled). The hardware thread indexing in MPI goes as follows: threads indexed from 0 till 19 
are located in physically different cores. The the next 20 threads overlap with the previous ones: 
20 is on the same core as 0, 21 with 1 etc...

Development
===========

Source code
~~~~~~~~~~~

tkwantoperator source code is available at https://gitlab.kwant-project.org/kwant/tkwantoperator. It can be cloned locally with git:

.. code:: shell

    $ git clone https://gitlab.kwant-project.org/kwant/tkwantoperator.git

The older commit history of the energy operators code can be found here: https://gitlab.kwant-project.org/spec-gmt/tkwant-energy-transport

Documentation
~~~~~~~~~~~~~

To build locally the documentation of ``tkwantoperator``, few additional (on top of the ones needed in :ref:`install`) python packages are required:

.. code:: shell

    $ pip3 install setuptools_scm sphinx jupyter-sphinx sphinx_autodoc_typehints

Latex is also needed to display the generated legends and axis titles in ``matplotlib`` plots:

* Ubuntu

    .. code:: shell

        $ sudo apt install texlive-latex-extra cm-super-minimal dvipng

* Fedora 

    .. code:: shell

        $ sudo dnf install texlive-scheme-medium

The documentation can then be build directly in the ``doc`` folder of the 
local tkwantoperator source code repository from the command line:

.. code:: shell

    $ cd path/to/tkwantoperator/tkwantoperator/doc
    $ make html

The generated HTML documentation will then be found in the ``doc/build/html`` folder. The root webpage is ``doc/build/html/index.html``, it can be opened with a web browser to browse the generated documentation.

Testing
~~~~~~~

If one made changes to the code of the energy operators and would like to know if the already present tests still pass successfully, 
one can use a specific virtual environment with the required packages, without ``tkwantoperator`` installed in it:

.. code:: shell

    $ virtualenv --system-site-packages tkwantoperator-dev
    $ source tkwantoperator-dev/bin/activate
    $ pip3 install --upgrade pytest setuptools_scm git+https://gitlab.kwant-project.org/kwant/tkwant.git

Then do an in-place compilation of the modified ``tkwantoperator`` source code to be finally able to run ``pytest``: 

.. code:: shell

    $ cd path/to/tkwantoperator-source-folder
    $ python3 setup.py build
    $ python3 setup.py build_ext -i
    $ pytest tkwantoperator
