About tkwantoperator
====================

``tkwantoperator`` is an opensource python package that extends the `tkwant <https://kwant-project.org/extensions/tkwant/>`_ library with additional operators. Currently, the extension includes energy related operators: current, density, source and heat current. ``tkwantoperator`` is distributed under the `2-clause BSD license  <https://gitlab.kwant-project.org/kwant/tkwantoperator/-/blob/master/LICENSE.rst>`_

Install instructions
~~~~~~~~~~~~~~~~~~~~
Complete install instructions can be found `here <https://kwant-project.org/extensions/tkwantoperator/pre/installation>`_ (or the file `INSTALL.rst <https://gitlab.kwant-project.org/kwant/tkwantoperator/-/blob/master/INSTALL.rst>`_ in the git repository).

Documentation
~~~~~~~~~~~~~
The documentation of this package, along with a tutorial, is available at : https://kwant-project.org/extensions/tkwantoperator