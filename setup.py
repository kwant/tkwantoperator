#!/usr/bin/env python3

from setuptools import setup
from Cython.Build import cythonize
from setuptools_scm import get_version

def readme():
    with open('README.rst') as f:
        return f.read()

requirements = (    
    "numpy>=1.8.2",
    "tinyarray",
    "kwant>=1.4,<2.0",
    "tkwant"
)

setup(name='tkwantoperator',
      version=get_version(),
      description='Additional operators for tkwant',
      long_description=readme(),
      long_description_content_type="text/x-rst",
      url='https://gitlab.kwant-project.org/kwant/tkwantoperator',
      author='tkwantoperator authors',
      author_email='tkwantoperator-authors@kwant-project.org',
      ext_modules = cythonize("tkwantoperator/energy.pyx", language_level=3),
      license='BSD',
      packages=['tkwantoperator'],
      install_requires=requirements,
      include_package_data=True,
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      zip_safe=False)