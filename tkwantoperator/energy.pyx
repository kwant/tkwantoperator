# -*- coding: utf-8 -*-
# Copyright 2020 tkwantoperator authors.


import cython

import tinyarray as ta
import kwant
import tkwant
import numpy as np

from typing import Union, Iterable, Tuple, List, Callable, Optional, Sequence, Dict

from kwant.graph.defs import gint_dtype
from kwant.graph.defs cimport gint

from kwant.operator cimport _get_orbs
from kwant.operator import _get_tot_norbs
from kwant._common import deprecate_args

from kwant.operator cimport Current as ParticleCurrent
from tkwant import _common
from tkwant import onebody


__all__ = ['EnergyCurrent', 'EnergyCurrentDivergence', 'EnergySource', 'EnergyDensity', 'LeadHeatCurrent', 'EnergySiteValidator']


class EnergySiteValidator:
    r"""
    A class that tells if energy related quantities can be calculated on a given site or hopping.

    In principle any site/hopping can be used, though (t)Kwant's implementation doesn't enable accessing the wave function on lead sites. Thus, if an energy operator is calculated on a site/hopping that involves lead sites as neighbors it would return wrong results (energy operators test against that use case to throw an exception).

    Parameters
    ----------
    syst : `~kwant.system.FiniteSystem`
        The finalized Kwant system for which sites/hoppings are checked.

    
    .. rubric:: Attributes

    Attributes
    ----------
    syst : `~kwant.system.FiniteSystem`
        The finalized system for which sites/hoppings are checked.
    
    interface_site_ids : list[int]
        List of all lead-interface sites

    interface_site_and_neighbors_ids : list[int]
        List of all lead-interface sites and their neighbors
       
    """
    @cython.embedsignature
    def __init__(self, syst: kwant.system.FiniteSystem):      
        if not isinstance(syst, kwant.system.FiniteSystem):
            raise ValueError("The argument given to 'syst' is not a kwant.system.FiniteSystem class instance.")
        self.syst = syst
        
        self.interface_site_ids = {site_id for interface in syst.lead_interfaces for site_id in interface}
        
        self.interface_site_and_neighbors_ids = self.interface_site_ids.copy()
        for interface_site_id in self.interface_site_ids:
            self.interface_site_and_neighbors_ids.update(self.syst.graph.out_neighbors(interface_site_id))

   
    def _validate_site_type(self, site):
        if isinstance(site, (int, np.integer)):
            if not (0 <= site < len(self.syst.sites)):
                raise ValueError("The integer value given in 'site' is not a valid site id.")    
        elif not isinstance(site, kwant.builder.Site):
            raise ValueError("The argument given as `site' is not a kwant.builder.Site class instance.")

    
    def _get_site_id(self, site):
        site_id = 0
        if isinstance(site, (int, np.integer)):
            site_id = site
        else:
            site_id = self.syst.id_by_site[site]
        
        return site_id


    @cython.embedsignature
    def is_site_valid_for_divergence(self, site: Union[int, kwant.builder.Site]) -> bool:
        r"""
        Tells if the energy current divergence can be calculated correctly in the given site. 
        
        Calculating the energy current divergence on lead-interface sites and their first nearest-neighbors would give wrong results.

        Parameters
        ----------
        site : `~kwant.builder.Site` or :py:class:`int`
            The Site to validate
        
        Returns
        -------
        bool
            Whether the site is valid of a divergence calculation.
        """
        self._validate_site_type(site)        
        return self._get_site_id(site) not in self.interface_site_and_neighbors_ids

    
    @cython.embedsignature
    def is_site_valid_for_density(self, site: Union[int, np.integer, kwant.builder.Site]) -> bool:
        r"""
        Tells if the energy density or source can be calculated correctly in the given site. 
        
        Calculating the energy density on lead-interface sites would give wrong results.
        
        Parameters
        ----------
        site : `~kwant.builder.Site` or :py:class:`int`
            The Site to validate
        
        Returns
        -------
        :py:class:`bool`
            Whether the site is valid for a density calculation.
        """
        
        self._validate_site_type(site)
        return self._get_site_id(site) not in self.interface_site_ids


    @cython.embedsignature
    def is_hopping_valid_for_current(self, 
            hopping: Union[Tuple[kwant.builder.Site, kwant.builder.Site], Tuple[int, int]]) -> bool:
        r"""
        Returns if the energy current can be calculated correctly between the two sites of the given hopping. Sites at the interface with leads would give wrong results.

        Parameters
        ----------
        hopping : :py:class:`tuple` [`~kwant.builder.Site`, `~kwant.builder.Site`] or :py:class:`tuple` [:py:class:`int`, :py:class:`int`]
            The hopping to validate
        
        Returns
        -------
        bool
            Whether the hopping is valid for an energy current calculation.
        """
        normalized_hop = kwant.operator._normalize_hopping_where(self.syst, [hopping])[0]

        return normalized_hop[0] not in self.interface_site_ids and normalized_hop[1] not in self.interface_site_ids

cdef class EnergyCurrent:

    r"""
    An operator for calculating the local energy current between two sites.

    The value of the energy current between two sites has no physical meaning as it is not uniquely defined. However, the total outgoing current from a site (*i.e.* the sum of the energy currents 
    from a site :math:`i` to all its neighbors :math:`j`), calculated by the class 
    `~tkwantoperator.EnergyCurrentDivergence`, is uniquely defined.
    """

    @cython.embedsignature
    def __init__(self,\
                 syst: kwant.system.FiniteSystem,\
                 where: Union[List[Tuple[int, int]], List[Tuple[kwant.builder.Site, kwant.builder.Site]]],\
                 baseOperator: str = "Kinetic+",\
                 customOnsite: Optional[Callable[[kwant.builder.Site, dict], complex]] = None,\
                 sum: bool = False):
        r"""
        Parameters
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system for which energy currents will be calculated.

        where : :py:class:`list` [(:py:class:`int`, :py:class:`int`)] or :py:class:`list` [(`~kwant.builder.Site`, `~kwant.builder.Site`)]
            The hoppings on which the current will be evaluated.  It is 
            saved in a normalized way, with the same order, in the `where` 
            attribute of this class.

        baseOperator : :py:class:`str`
            Can be ``Hamiltonian``, ``Kinetic+`` or ``Custom`` (case unsensitive). 
            It tells the operator which onsite term to use when calculating the 
            energy current. Saved as an integer in `operatorType`

        customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Overwrites the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, 
            but only when ``baseOperator`` is set to ``Custom``. In practice, its signature 
            should be ``customOnsite(site, params)`` where ``site`` is a `~kwant.builder.Site` 
            instance and ``params`` is a :py:class:`dict` containing the parameters 
            values (including the current time). The method must return a matrix-like
            object (e.g. a `~tinyarray` instance) of the right dimensions at each site 
            (the same as the onsite Hamiltonian). It is saved in the `customOnsite` 
            attribute of this class.

        sum : :py:class:`bool`
            Whether to sum the currents over each hoppings of the `where` list and return a 
            single float result when called. Saved as an integer in the `sum` attribute of 
            this class.


        Notes
        -----
        This operator is used to calculate the energy current between each pair of sites :math:`(j, i)`
        in `where`. At a given time :math:`t`, the energy current through a hopping :math:`(j, i)`
        (from site :math:`i` to site :math:`j`) writes:        

        .. math::
            I_{ji}^ε(t) = \sum_{\text{lead } α} \; \sum_{{\text{mode } m_α}} \int \frac{{\text{d}E}}{2\pi} f_α(E) \sum_{k} - \text{Im} \left [ \left [ ψ_k^{m_α E} \right ]^\dagger ε_{ki} ε_{ij} ψ_j^{m_α E} - \left [ ψ_k^{m_α E} \right ]^\dagger ε_{kj} ε_{ji} ψ_i^{m_α E} \right ]

        The above formula has been derived in :ref:`article-ref` for single-orbital Hamiltonians. In practice, the operator `EnergyCurrent` has been implemented for generic multi-orbital Hamiltonian matrices but the physical meaning of the multi-orbital formula is not clear in general. It has not yet been investigated.
        :math:`ε` is the energy operator derived from the system's Hamiltonian:

        * Its values on hoppings coincide with the Hamiltonian        
          :math:`\forall i \! \neq \! j \; ε_{ij}(t) = H_{ij}(t)`
        * Its value on sites depends on what the user chooses for ``baseOperator``:

          * ``Hamiltonian``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t)`
          * ``Kinetic+``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t_0 - 1)` where 
            :math:`t_0` is the instant at which the Hamiltonian starts being time-dependent, 
            given by `time_start` (the :math:`-1` has been added to avoid use-case problems 
            with heaviside-like time-dependence).
          * ``Custom``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = W_{i}(t)` where 
            :math:`W` is given by the user through ``customOnsite``.
            Note that it is necessary to impose :math:`ε_{ii}(t) = H_{ii}(t_0 - 1)` for all sites :math:`i` (belonging to `added_lead_sites`)
            that were part of the leads and have been added into the central system. 
            Be cautious that there is currently no implemented check to verify this condition and if it is not satisfied, the output result of `Current` might be wrong.

        The index :math:`k` runs formally over all sites in the whole system,
        but effectively only the sites connected to :math:`i` and :math:`j` (including :math:`i` and :math:`j`) 
        give a non zero result.

        .. rubric:: Attributes

        Attributes
        ----------
        sum : :py:class:`int`, :math:`0` (no) or :math:`1` (yes)
            Whether to sum the currents over each hoppings of the `where` list and return a 
            single float result.

        where_size : :py:class:`int`
            Number of hoppings in the `where` list: the size of its first dimension.

        operatorType : :py:class:`int`
            Translation to an integer of the ``baseOperator`` parameter of the class
            constructor: "Hamiltonian" :math:`\leftrightarrow 0`, 
            "kinetic+" :math:`\leftrightarrow 1` and "custom" :math:`\leftrightarrow 2`.

        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which energy currents will be calculated.

        customOnsite : :py:const:`None` or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Used as the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, when
            `operatorType` = 2.

        time_name : :py:class:`str`
            The name the time parameter has in the ``params`` dictionary given to `__call__`. It
            is initialised with tkwant's default name, which is ``time``.

        time_start : :py:class:`float`
            The instant :math:`t_0` at which time dependence starts in the Hamiltonian. 
            Initialized with tKwant's default value, which is ``0``.

        where : 2D array of `~kwant.graph.defs.gint` (a python :py:class:`int` compatible type) of shape :math:`n \times 2`
            List of hoppings :math:`(j, i)` (accessed as ``j, i = where[n, 0], where[n, 1]``) 
            on which the energy current is calculated at each call.

        _neighbors_i : :py:class:`list` of `~numpy.array` of :py:class:`int`
            List of lists of the neighbors of each second site :math:`i` of each hopping :math:`(j, i)` in `where`

        _neighbors_j : :py:class:`list` of `~numpy.array` of :py:class:`int`
            List of lists of the neighbors of each first site :math:`j` of each hopping :math:`(j, i)` in `where`
        """
        if baseOperator.lower() == "hamiltonian":
            self.operatorType = 0
            # 0 will refer to hamiltonian based operators, is there any #define equivalent in Cython ?
        elif baseOperator.lower() == "kinetic+":
            self.operatorType = 1
        elif baseOperator.lower() == "custom":
            if not customOnsite:
                raise ValueError('customOnsite has not been specified')
            elif not callable(customOnsite):
                raise ValueError('customOnsite has to be a callable per site')
            else:
                self.operatorType = 2
                self.customOnsite = customOnsite
        else:
            raise ValueError('The argument given to \'baseOperator\' is not supported')
        
        self.sum = sum
        self.syst = syst
        self.where = kwant.operator._normalize_hopping_where(syst, where)
        self._site_ranges = np.asarray(syst.site_ranges, dtype=gint_dtype)
        self.where_size = self.where.shape[0]
        self._neighbors_i = list()
        self._neighbors_j = list()
        self._static_onsite = dict()
        self._saved_static_onsite = 0

        self._check_if_hopping_is_at_interface()
        self._update_neighbor_lists()

        self.time_name = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_name')
        self.time_start = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_start')

    def _check_if_hopping_is_at_interface(self):
        interface_site_ids = {site_id for interface in self.syst.lead_interfaces for site_id in interface}
        for hopping in self.where:
            i = hopping[1]
            j = hopping[0]
            if (i in interface_site_ids) or (j in interface_site_ids):
                raise ValueError("The user supplied where list contains a hopping that has a site that is at the interface with a lead: calculating currents from those sites gives wrong results.")

    def _update_neighbor_lists(self):
        
        self._neighbors_i.clear()
        self._neighbors_j.clear()
        
        for index, hopping in enumerate(self.where):
            i = hopping[1]
            j = hopping[0]
            assert(isinstance(i, (int, np.integer)))
            assert(isinstance(j, (int, np.integer)))

            self._neighbors_i.append(np.asarray([k for k in self.syst.graph.out_neighbors(i)], dtype=gint_dtype))
            self._neighbors_j.append(np.asarray([k for k in self.syst.graph.out_neighbors(j)], dtype=gint_dtype))
    
    def _save_static_onsite(self, params=None):
        init_params = params.copy()
        init_params[self.time_name] = self.time_start - 1.0

        for hopping_index in range(self.where_size):
            i = self.where[hopping_index, 1]           
            j = self.where[hopping_index, 0]

            if i not in self._static_onsite.keys():
                self._static_onsite[i] = ta.matrix(self.syst.hamiltonian(i, i, params=init_params), complex)            
            
            if j not in self._static_onsite.keys():
                self._static_onsite[j] = ta.matrix(self.syst.hamiltonian(j, j, params=init_params), complex) 

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.embedsignature
    def __call__(self, 
                 psi: Sequence[complex],
                 params: Optional[Dict[str, Union[float, complex]]] = None) -> Union[float, List[float]]:
        r"""Returns the contribution :math:`I_{ji}^ε(ψ, t)` of ``psi`` to the expectation value
        of the current flowing through the hoppings given in `where`.

        This method enables calling an instance `A` of this class like a 
        function, with a wave function and the "params" dictionary as parameters :

        >>> A(psi, params)

        For each hopping :math:`(j,i)` it calculates the following sum, with ``psi``
        written as ":math:`ψ`" :

        .. math::
            I_{ji}^ε(ψ, t) = \sum_{k} - \text{Im} \left [ ψ_k^\dagger ε_{ki} ε_{ij} ψ_j - ψ_k^\dagger ε_{kj} ε_{ji} ψ_i \right ]
        
        Parameters
        ----------
        psi : array of complex values
            The wave function to use for calculation. Must have the same length as the total 
            number of orbitals in the system.

        params : :py:class:`dict`, optional
            Dictionary of parameter names and their values.

        Returns
        -------
        :py:class:`list` [:py:class:`float`] or :py:class:`float`
            List of the values fo the contribution of ``psi`` to the currents on each hopping :math:`\left [ I_{ji}^ε(ψ, t) \right ]_{(j,i) \in \text{where}}`, when `sum` :math:`= 0`. 
            Otherwise the sum of these values :math:`\sum_{(j,i) \in \text{where}} I_{ji}^ε(ψ, t)`.
        """
        cdef gint i, norbs_i, offset_i
        cdef gint j, norbs_j, offset_j
        cdef gint k, norbs_k, offset_k, index_k
        cdef gint[:] neighbors_k_of_i, neighbors_k_of_j
        cdef int hopping_index    
        cdef gint[:,:] where = self.where  
     
        if psi is None:
            raise TypeError('psi must be an array')        

        psi = np.asarray(psi, dtype=complex)      
        
        tot_norbs = _get_tot_norbs(self.syst)
        
        if psi.shape != (tot_norbs,):
            raise ValueError('psi vector is of incorrect shape')      

        if not self._saved_static_onsite:
            self._saved_static_onsite = 1
            self._save_static_onsite(params=params)     

        cdef complex[:] wf = psi

        result = np.zeros(self.where_size, dtype=float)        
        
        # main loop         
        for hopping_index in range(where.shape[0]):      

            i = where[hopping_index, 1]            
            _get_orbs(self._site_ranges, i, &offset_i, &norbs_i)
           
            j = where[hopping_index, 0]           
            _get_orbs(self._site_ranges, j, &offset_j, &norbs_j)

            psi_i = ta.array(wf[offset_i:(offset_i + norbs_i)])
            psi_i_conj = ta.conjugate(psi_i)

            psi_j = ta.array(wf[offset_j:(offset_j + norbs_j)])
            psi_j_conj = ta.conjugate(psi_j)

            E_ii = None
            E_jj = None

            if self.operatorType == 0:
                # Use the hamiltonian as onsite
                E_ii = ta.matrix(self.syst.hamiltonian(i, i, params=params), complex)
                E_jj = ta.matrix(self.syst.hamiltonian(j, j, params=params), complex)
            elif self.operatorType == 1:
                # We get the hamiltonian at instant 0 as onsite
                E_ii = self._static_onsite[i]
                E_jj = self._static_onsite[j]
            else: 
                # Use user provided onsite
                E_ii = ta.matrix(self.customOnsite(self.syst.sites[i], params=params), complex)
                E_jj = ta.matrix(self.customOnsite(self.syst.sites[j], params=params), complex)

            # When j != i we have H_ji = ε_ji
            E_ij = ta.matrix(self.syst.hamiltonian(i, j, params=params), complex)
            E_ji = ta.conjugate(ta.transpose(E_ij))

            # Calculating this first sum :math:`\sum_k {ψ_k^\dagger ε_{ki} ε_{ij} ψ_j
            neighbors_k_of_i = self._neighbors_i[hopping_index]

            # Add first the term when k=i, not in the neighbors list
            result[hopping_index] -= ta.dot(ta.dot(ta.dot(psi_i_conj, E_ii), E_ij), psi_j).imag

            for index_k in range(neighbors_k_of_i.shape[0]):
                k = neighbors_k_of_i[index_k]
                _get_orbs(self._site_ranges, k, &offset_k, &norbs_k)

                psi_k_conj = ta.conjugate(ta.array(wf[offset_k:(offset_k + norbs_k)], complex))
                
                # When k != i we have H_ki = ε_ki
                E_ki = ta.matrix(self.syst.hamiltonian(k, i, params=params), complex)             

                result[hopping_index] -= ta.dot(ta.dot(ta.dot(psi_k_conj, E_ki), E_ij), psi_j).imag            

            # Calculating the second sum :math:`\sum_k {ψ_k^\dagger ε_{kj} ε_{ji} ψ_i
            neighbors_k_of_j = self._neighbors_j[hopping_index]

            # Subtract first the term when k=j, not in the neighbors list
            result[hopping_index] += ta.dot(ta.dot(ta.dot(psi_j_conj, E_jj), E_ji), psi_i).imag

            for index_k in range(neighbors_k_of_j.shape[0]):
                k = neighbors_k_of_j[index_k]
                _get_orbs(self._site_ranges, k, &offset_k, &norbs_k)

                psi_k_conj = ta.conjugate(ta.array(wf[offset_k:(offset_k + norbs_k)], complex))

                # When k != i we have H_ki = ε_ki
                E_kj = ta.matrix(self.syst.hamiltonian(k, j, params=params), complex)

                result[hopping_index] += ta.dot(ta.dot(ta.dot(psi_k_conj, E_kj), E_ji), psi_i).imag
                        
        return np.sum(result) if self.sum else result


cdef class EnergySource:

    r"""
    An operator for calculating the local input power on sites. 
    """

    @cython.embedsignature
    def __init__(self,
                 syst: kwant.system.FiniteSystem,
                 where: Union[List[int], List[kwant.builder.Site]],
                 baseOperator: str ="Kinetic+",
                 dt_hamiltonian: Optional[Callable[[Tuple[kwant.builder.Site, kwant.builder.Site], Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 dt_customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 sum: bool =False,
                 dt: float =1E-3):

        r"""
        Parameters
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which the input power will be calculated.

        where : :py:class:`list` [:py:class:`int`] or :py:class:`list` [`~kwant.builder.Site`]
            The sites on which the input power will be evaluated.  It is 
            saved in a normalized way, with the same order, in the `where` 
            attribute of this class.

        baseOperator : :py:class:`str`
            Can be ``Hamiltonian``, ``Kinetic+`` or ``Custom`` (case unsensitive). 
            It tells the operator which onsite term to use when calculating the 
            energy current. Saved as an integer in `operatorType`

        customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Overwrites the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, 
            but only when ``baseOperator`` is set to ``Custom``. In practice, its signature 
            should be ``customOnsite(site, params)`` where ``site`` is a `~kwant.builder.Site` 
            instance and ``params`` is a :py:class:`dict` containing the parameters 
            values (including the current time). The method must return a matrix-like
            object (e.g. a `~tinyarray` instance) of the right dimensions at each site 
            (the same as the onsite Hamiltonian). It is saved in the `customOnsite` 
            attribute of this class.

        sum : :py:class:`bool`
            Whether to sum the input powers over the `where` list and return a 
            single float result when called. Saved as an integer in the `sum` attribute of 
            this class.

        dt_customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            The time derivative of the custom onsite, to be (optionally) given only 
            when ``baseOperator`` is set to ``Custom``. Saved in the `dt_customOnsite` 
            attribute of this class. If specified, its signature 
            should be ``dt_customOnsite(site, params)`` where ``site`` is a
            `~kwant.builder.Site` instance and ``params`` is a :py:class:`dict`
            containing the parameters values (including the current time). 
            The method must return a matrix-like object (e.g. a `tinyarray` 
            instance) of the right dimensions at each site (the same as the 
            onsite Hamiltonian). If not specified (thus 
            defaulting to :py:const:`None`), finite difference will be used 
            to estimate the time derivative using the "small" time step `dt`.         

        dt_hamiltonian : :py:const:`None` (default) or (`~kwant.builder.Site`, `~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            The time derivative of the Hamiltonian. Saved in the `dt_hamiltonian` 
            attribute of this class. If specified, its signature 
            should be ``dt_hamiltonian(site1, site2, params)`` where ``site1`` and 
            ``site2`` are `~kwant.builder.Site` instances ; ``params`` is a :py:class:`dict` 
            containing the parameters values (including the current time). The method 
            must return a matrix-like object (e.g. a `tinyarray` instance) of the right 
            dimensions for each hopping ``(site1, site2)`` (the same size as the 
            Hamiltonian). If not specified (thus defaulting to :py:const:`None`), 
            finite difference will be used to estimate it using the "small" time step `dt`.

        dt : :py:class:`float`, default value: :math:`10^{-3}`
            The time step used to estimate the time derivative of the custom onsite (if ``dt_customOnsite=None``)
            or the Hamiltonian (if ``dt_hamiltonian=None``). It is saved in 
            the `dt` attribute of this class. 

        Notes
        -----
        This class calculates the power source term due to the time
        dependent potentials at each site :math:`i`
        given in the `where` list. At a given time :math:`t`, the power given to particles
        at site :math:`i` writes:

        .. math::
            S_i^ε(t) = \sum_{\text{lead } α} \; \sum_{{\text{mode } m_α}} \int \frac{{\text{d}E}}{2\pi} f_α(E) \sum_{k} \left\{- \text{Im} \left [ \left [ ψ_i^{m_α E} \right ]^\dagger V_i  ε_{ik} ψ_k^{m_α E} + \left [ ψ_k^{m_α E} \right ]^\dagger V_k ε_{ki} ψ_i^{m_α E} \right ] + \text{Re} \left [ \left [ ψ_i^{m_α E} \right ]^\dagger ∂_t ε_{ik} ψ_k^{m_α E} \right ]\right\}

        The above formula has been derived in :ref:`article-ref` for single-orbital Hamiltonians. In practice, the operator `EnergySource` has been implemented for generic multi-orbital Hamiltonian matrices but the physical meaning of the multi-orbital formula is not clear in general. It has not yet been investigated.
        :math:`ε` is the energy operator derived from the system's Hamiltonian:

        * Its values on hoppings coincide with the hamiltonian        
          :math:`\forall i \! \neq \! j \; ε_{ij}(t) = H_{ij}(t)`
        * Its value on sites depends on what the user chooses for ``baseOperator``:

          * ``Hamiltonian``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t)`
          * ``Kinetic+``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t_0 - 1)` where 
            :math:`t_0` is the instant at which the Hamiltonian starts being time-dependent, 
            given by `time_start` (the :math:`-1` has been added to avoid use-case problems 
            with heaviside-like time-dependence).
          * ``Custom``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = W_{i}(t)` where 
            :math:`W` is given by the user through ``customOnsite``.
            Note that it is necessary to impose :math:`ε_{ii}(t) = H_{ii}(t_0 - 1)` for all sites :math:`i` (belonging to `added_lead_sites`)
            that were part of the leads and have been added into the central system. 
            Be cautious that there is currently no implemented check to verify this condition and if it is not satisfied, the output result of `Source` might be wrong.

        :math:`V` is defined by
        :math:`V_i(t) = H_{ii}(t) - ε_{ii}(t)`. The index :math:`k` runs formally over all sites in the whole system,
        but effectively only the sites connected to :math:`i` (including :math:`i`) 
        give in general a non zero result.

        .. rubric:: Attributes

        Attributes
        ----------
        sum : :py:class:`int`, :math:`0` (no) or :math:`1` (yes)
            Whether to sum the input power over the `where` list and return a 
            single float result.

        where_size : :py:class:`int`
            Number of sites in the `where` list: the size of its first dimension.

        operatorType : :py:class:`int`
            Translation to an integer of the ``baseOperator`` parameter of the class
            constructor: "Hamiltonian" :math:`\leftrightarrow 0`, 
            "kinetic+" :math:`\leftrightarrow 1` and "custom" :math:`\leftrightarrow 2`.

        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which energy currents will be calculated.

        customOnsite : :py:const:`None` or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Used as the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, when
            `operatorType` = 2.

        time_name : :py:class:`str`
            The name the time parameter has in the ``params`` dictionary given to `__call__`. It
            is initialised with tkwant's default name, which is ``time``.

        time_start : :py:class:`float`
            The instant :math:`t_0` at which time dependence starts in the Hamiltonian. 
            Initialized with tKwant's default value, which is ``0``.

        dt_customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Used as the value of the time derivative of the energy operator :math:`ε` 
            on sites: :math:`\forall i \, \forall t \; ~ ∂_t ε_{ii}(t) = \text{dt_customOnsite}(i, t)`, when
            `operatorType` = 2.

        dt_hamiltonian : :py:const:`None` (default) or (`~kwant.builder.Site`, `~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            The time derivative of the Hamiltonian. If it is set to ``None``, 
            finite difference will be used to estimate the time derivative 
            of the Hamiltonian, using the "small" time step `dt`.

        dt : :py:class:`float`, default value: :math:`10^{-3}`
            The time step used to estimate the time derivative of the custom onsite
            or the Hamiltonian, if not user specified.

        where : 2D array of `~kwant.graph.defs.gint` (a python :py:class:`int` compatible type) of shape :math:`n \times 2`
            List of sites :math:`i` (accessed as ``i = where[n, 0]``) on which the energy source is calculated at each call.

        _neighbors_i : :py:class:`list` of `~numpy.array` of :py:class:`int`
            List of lists of the neighbors of each site :math:`i` in `where`

        """
        
        if baseOperator.lower() == "hamiltonian":
            self.operatorType = 0
            # 0 will refer to hamiltonian based operators, is there any #define equivalent in Cython ?
        elif baseOperator.lower() == "kinetic+":
            self.operatorType = 1
        elif baseOperator.lower() == "custom":
            if not customOnsite:
                raise ValueError('customOnsite has not been specified')
            elif not callable(customOnsite):
                raise ValueError('customOnsite has to be a callable per site')
            else:
                self.operatorType = 2
                self.customOnsite = customOnsite
        else:
            raise ValueError('The argument given to \'baseOperator\' is not supported')
       
        self.sum = sum
        self.dt = dt
        self.has_dt_hamiltonian = 1 if dt_hamiltonian else 0
        self.dt_hamiltonian = dt_hamiltonian

        self.has_dt_customOnsite = 1 if dt_customOnsite else 0
        self.dt_hamiltonian = dt_hamiltonian        
        
        self.syst = syst
        self.where = kwant.operator._normalize_site_where(syst, where)
        self._check_if_where_has_interface_sites()

        self._site_ranges = np.asarray(syst.site_ranges, dtype=gint_dtype)
        self.where_size = self.where.shape[0]
        self._neighbors_i = list()
        self._static_onsite = dict()
        self._saved_static_onsite = 0

        self._update_neighbor_lists()

        self.time_name = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_name')
        self.time_start = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_start')

    
    def _check_if_where_has_interface_sites(self):
        interface_site_ids = {site_id for interface in self.syst.lead_interfaces for site_id in interface}

        for hopping in self.where:
            i = hopping[0]
            if i in interface_site_ids:
                raise ValueError("The energy source class has been instanced with a `where' that contains sites at the interface with leads. Calculating energy related quantities on those sites give wrong results.")
    
    def _update_neighbor_lists(self):        
        self._neighbors_i.clear()        
        for hopping in self.where:   
            i = hopping[0]        
            assert(isinstance(i, (int, np.integer)))
            self._neighbors_i.append(np.asarray([k for k in self.syst.graph.out_neighbors(i)], dtype=gint_dtype))      

    def _save_static_onsite(self, params=None):
        init_params = params.copy()
        init_params[self.time_name] = self.time_start - 1.0

        for hopping_index, hopping in enumerate(self.where): 
            i = hopping[0]
            if i not in self._static_onsite.keys():
                self._static_onsite[i] = ta.matrix(self.syst.hamiltonian(i, i, params=init_params), complex)            
            
            for k in self._neighbors_i[hopping_index]:
                if k not in self._static_onsite.keys():
                    self._static_onsite[k] = ta.matrix(self.syst.hamiltonian(k, k, params=init_params), complex)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.embedsignature
    def __call__(self, 
                 psi: Sequence[complex],
                 params: Optional[Dict[str, Union[float, complex]]] = None) -> Union[float, List[float]]:
        r"""Returns the contribution :math:`S_i^ε(ψ, t)` of ``psi`` to the expectation value
        of the power given to the particles on each site of the `where` list.

        This method enables calling an instance `A` of this class like a 
        function, with a wave function and the "params" dictionary as parameters :

        >>> A(psi, params)

        For each site :math:`i` it calculates the following sum, with ``psi``
        written as ":math:`ψ`" :

        .. math::
            S_i^ε(ψ, t) = \sum_{k} - \text{Im} \left [ψ_i^\dagger V_i  ε_{ik} ψ_k + ψ_k^\dagger V_k ε_{ki} ψ_i \right ] + \text{Re} \left [ ψ_i^\dagger ∂_t ε_{ik} ψ_k \right ]

        Parameters
        ----------
        psi : array of complex values
            The wave function to use for calculation. Must have the same length as the total 
            number of orbitals in the system.

        params : :py:class:`dict`, optional
            Dictionary of parameter names and their values.

        Returns
        -------
        :py:class:`list` [:py:class:`float`] or :py:class:`float`
            List of the values of the contribution of ``psi`` to the input power on each site 
            :math:`\left [ S_i^ε(ψ, t) \right ]_{i \in \text{where}}`, 
            when `sum` :math:`= 0`. Otherwise the sum of these values 
            :math:`\sum_{i \in \text{where}} S_i^ε(ψ, t)`.
        """
        cdef gint i, norbs_i, offset_i       
        cdef gint k, norbs_k, offset_k, index_k
        cdef gint[:] neighbors_k_of_i
        cdef int hopping_index   
     
        if psi is None:
            raise TypeError('psi must be an array') 

        if not self._saved_static_onsite:
            self._saved_static_onsite = 1
            self._save_static_onsite(params=params)       

        psi = np.asarray(psi, dtype=complex)      
        
        tot_norbs = _get_tot_norbs(self.syst)
        
        if psi.shape != (tot_norbs,):
            raise ValueError('psi vector is of incorrect shape')      

        cdef complex[:] wf = psi

        result = np.zeros(self.where_size, dtype=float)        
        
        # main loop         
        for hopping_index in range(self.where_size):      

            i = self.where[hopping_index, 0]                 
            #print('i: ', i)
            _get_orbs(self._site_ranges, i, &offset_i, &norbs_i)          

            psi_i = ta.array(wf[offset_i:(offset_i + norbs_i)])

            #print('ψ_i: ', psi_i)
            psi_i_conj = ta.conjugate(psi_i)           

            V_i = None           
            dt_E_ii = None
          
            if self.operatorType == 0:
                # Use the hamiltonian as onsite          
                
                # In this case there's no 'time dependent part' of the hamiltonian cf docstring
                # V_i = 0
                if self.has_dt_hamiltonian:
                    dt_E_ii = ta.matrix(self.dt_hamiltonian(self.syst.sites[i], self.syst.sites[i], params=params), complex)
                else:
                    params_plus_dt = params.copy()
                    params_plus_dt[self.time_name] += self.dt

                    params_minus_dt = params.copy()
                    params_minus_dt[self.time_name] -= self.dt

                    dt_E_ii = (ta.matrix(self.syst.hamiltonian(i, i, params=params_plus_dt), complex) -\
                               ta.matrix(self.syst.hamiltonian(i, i, params=params_minus_dt), complex)) / (2*self.dt)               

            elif self.operatorType == 1:
                # We get the hamiltonian at instant 0 as onsite      
                V_i = ta.matrix(self.syst.hamiltonian(i, i, params=params), complex) - self._static_onsite[i]
                
                # In the kinetic+ case the onsite has no time derivative
                # dt_E_ii = 0

            else: 
                # Use user provided onsite                
                site_i = self.syst.sites[i] 
                V_i = ta.matrix(self.syst.hamiltonian(i, i, params=params), complex) -\
                        ta.matrix(self.customOnsite(site_i, params=params), complex)
                if self.has_dt_customOnsite:
                    dt_E_ii = ta.matrix(self.dt_customOnsite(site_i, params=params), complex)
                else:
                    params_plus_dt = params.copy()
                    params_plus_dt[self.time_name] += self.dt

                    params_minus_dt = params.copy()
                    params_minus_dt[self.time_name] -= self.dt

                    dt_E_ii = (ta.matrix(self.customOnsite(site_i, params=params_plus_dt), complex) -\
                               ta.matrix(self.customOnsite(site_i, params=params_minus_dt), complex)) / (2*self.dt)


            # Add first the term when k=i, not in the neighbors list. That term can be non zero
            # only if it is a customOnsite provided by the user.
            if self.operatorType == 0 or self.operatorType == 2:
                #print('dt_E_ii: ', dt_E_ii)
                result[hopping_index] += ta.dot(psi_i_conj, ta.dot(dt_E_ii, psi_i)).real

            
            neighbors_k_of_i = self._neighbors_i[hopping_index]

            for index_k in range(neighbors_k_of_i.shape[0]):
                k = neighbors_k_of_i[index_k]                               
                #print('k: ', k)
                _get_orbs(self._site_ranges, k, &offset_k, &norbs_k)

                psi_k = ta.array(wf[offset_k:(offset_k + norbs_k)], complex)
                #print('ψ_k: ', psi_k)               
                
                if self.operatorType == 1 or self.operatorType == 2:                    
                    psi_k_conj = ta.conjugate(psi_k)

                    # Since i != k, we have E_ik == H_ik
                    H_ik = ta.matrix(self.syst.hamiltonian(i, k, params=params), complex)
                    H_ki = ta.conjugate(ta.transpose(H_ik))       

                    #print('H_ik: ', H_ik)             

                    V_k = None                    
                    if self.operatorType == 1:
                        V_k = ta.matrix(self.syst.hamiltonian(k, k, params=params), complex) - self._static_onsite[k]
                    else:
                        V_k = ta.matrix(self.syst.hamiltonian(k, k, params=params), complex) -\
                                ta.matrix(self.customOnsite(self.syst.sites[k] , params=params), complex)                    

                    #print('V_k: ', V_k)
                    result[hopping_index] -= (ta.dot(psi_i_conj, ta.dot(V_i, ta.dot(H_ik, psi_k))) +\
                                                 ta.dot(psi_k_conj, ta.dot(V_k, ta.dot(H_ki, psi_i)))).imag
                
                
                # Since i != k, we have E_ik == H_ik
                dt_E_ik = None
                
                if self.has_dt_hamiltonian:
                    dt_E_ik = ta.matrix(self.dt_hamiltonian(self.syst.sites[i], self.syst.sites[k], params=params), complex)
                else:
                    params_plus_dt = params.copy()
                    params_plus_dt[self.time_name] += self.dt

                    params_minus_dt = params.copy()
                    params_minus_dt[self.time_name] -= self.dt

                    dt_E_ik = (ta.matrix(self.syst.hamiltonian(i, k, params=params_plus_dt), complex) -\
                        ta.matrix(self.syst.hamiltonian(i, k, params=params_minus_dt), complex)) / (2*self.dt)

                #print('dt_E_ik: ', dt_E_ik)
                result[hopping_index] += ta.dot(psi_i_conj, ta.dot(dt_E_ik, psi_k)).real
                        
        return np.sum(result) if self.sum else result




cdef class EnergyDensity:
    r"""
    An operator for calculating the local energy density on sites.
    """

    @cython.embedsignature
    def __init__(self,
                 syst: kwant.system.FiniteSystem,
                 where: Union[List[int], List[kwant.builder.Site]],
                 baseOperator: str ="Kinetic+",
                 customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 sum: bool =False):

        r"""
        Parameters
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which energy density will be calculated.

        where : :py:class:`list` [:py:class:`int`] or :py:class:`list` [`~kwant.builder.Site`]
            The sites on which the energy density will be evaluated. It is 
            saved in a normalized way, with the same order, in the `where` 
            attribute of this class.

        baseOperator : :py:class:`str`
            Can be ``Hamiltonian``, ``Kinetic+`` or ``Custom`` (case unsensitive). 
            It tells the operator which onsite term to use when calculating the 
            energy current. Saved as an integer in `operatorType`

        customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Overwrites the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, 
            but only when ``baseOperator`` is set to ``Custom``. In practice, its signature 
            should be ``customOnsite(site, params)`` where ``site`` is a `~kwant.builder.Site` 
            instance and ``params`` is a :py:class:`dict` containing the parameters 
            values (including the current time). The method must return a matrix-like
            object (e.g. a `~tinyarray` instance) of the right dimensions at each site 
            (the same as the onsite hamiltonian). It is saved in the `customOnsite` 
            attribute of this class.

        sum : :py:class:`bool`
            Whether to sum the energy densities over the `where` list and return a 
            single float result when called. Saved as an integer in the `sum` attribute of 
            this class.


        Notes
        -----
        This operator is used to calculate the energy density on each site :math:`i`
        in the `where` list. At a given time :math:`t`, the energy density 
        on a site :math:`i` writes:        

        .. math::
            ρ^ε_i(t) = \sum_{\text{lead } α} \; \sum_{{\text{mode } m_α}} \int \frac{{\text{d}E}}{2π} f_α(E) \sum_{j} \text{Re} \left [ \left [ ψ_i^{m_α E} \right ]^\dagger ε_{ij} ψ_j^{m_α E} \right ]

        The above formula has been derived in :ref:`article-ref` for single-orbital Hamiltonians. In practice, the operator `EnergyDensity` has been implemented for generic multi-orbital Hamiltonian matrices but the physical meaning of the multi-orbital formula is not clear in general. It has not yet been investigated.
        :math:`ε` is the energy operator derived from the system's Hamiltonian:

        * Its values on hoppings coincide with the Hamiltonian        
          :math:`\forall i \! \neq \! j \; ε_{ij}(t) = H_{ij}(t)`
        * Its value on sites depends on what the user chooses for ``baseOperator``:

          * ``Hamiltonian``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t)`
          * ``Kinetic+``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = H_{ii}(t_0 - 1)` where 
            :math:`t_0` is the instant at which the Hamiltonian starts being time-dependent, 
            given by `time_start` (the :math:`-1` has been added to avoid use-case problems 
            with heaviside-like time-dependence).
          * ``Custom``: :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = W_{i}(t)` where 
            :math:`W` is given by the user through ``customOnsite``.
            Note that it is necessary to impose :math:`ε_{ii}(t) = H_{ii}(t_0 - 1)` for all sites :math:`i` (belonging to `added_lead_sites`)
            that were part of the leads and have been added into the central system. 
            Be cautious that there is currently no implemented check to verify this condition and if it is not satisfied, the output result of `Density` might be wrong.

        The index :math:`j` runs formally over all sites in the whole system,
        but effectively only the sites connected to :math:`i` (including :math:`i`) 
        give in general a non zero result.

        .. rubric:: Attributes

        Attributes
        ----------
        sum : :py:class:`int`, :math:`0` (no) or :math:`1` (yes)
            Whether to sum the energy densities over the `where` list and return a 
            single float result.

        where_size : :py:class:`int`
            Number of sites in the `where` list: the size of its first dimension.

        operatorType : :py:class:`int`
            Translation to an integer of the ``baseOperator`` parameter of the class
            constructor: "hamiltonian" :math:`\leftrightarrow 0`, 
            "kinetic+" :math:`\leftrightarrow 1` and "custom" :math:`\leftrightarrow 2`.

        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which energy currents will be calculated.

        customOnsite : :py:const:`None` or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Used as the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, when
            `operatorType` = 2.

        time_name : :py:class:`str`
            The name the time parameter has in the ``params`` dictionary given to `__call__`. It
            is initialised with tkwant's default name, which is ``time``.

        time_start : :py:class:`float`
            The instant :math:`t_0` at which time dependence starts in the Hamiltonian. 
            Initialized with tKwant's default value, which is ``0``.

        where : 2D array of `~kwant.graph.defs.gint` (a python :py:class:`int` compatible type) of shape :math:`n \times 2`
            List of sites :math:`i` (accessed as ``i = where[n, 0]``) on which the energy density is calculated at each call.

        _neighbors_i : :py:class:`list` of `~numpy.array` of :py:class:`int`
            List of lists of the neighbors of each site :math:`i` in `where`
        """
        
        if baseOperator.lower() == "hamiltonian":
            self.operatorType = 0
            # 0 will refer to hamiltonian based operators, is there any #define equivalent in Cython ?
        elif baseOperator.lower() == "kinetic+":
            self.operatorType = 1
        elif baseOperator.lower() == "custom":
            if not customOnsite:
                raise ValueError('customOnsite has not been specified')
            elif not callable(customOnsite):
                raise ValueError('customOnsite has to be a callable per site')
            else:
                self.operatorType = 2
                self.customOnsite = customOnsite
        else:
            raise ValueError('The argument given to \'baseOperator\' is not supported')
       
        self.sum = sum       
        self.syst = syst
        self.where = kwant.operator._normalize_site_where(syst, where)
        self._check_if_where_has_interface_sites()

        self._site_ranges = np.asarray(syst.site_ranges, dtype=gint_dtype)
        self.where_size = self.where.shape[0]
        self._neighbors_i = list()

        self._static_onsite = dict()
        self._saved_static_onsite = 0

        self._update_neighbor_lists()

        self.time_name = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_name')
        self.time_start = _common.get_default_function_argument(onebody.WaveFunction.from_kwant, 'time_start')

    def _check_if_where_has_interface_sites(self):
        interface_site_ids = {site_id for interface in self.syst.lead_interfaces for site_id in interface}

        for hopping in self.where:
            i = hopping[0]
            if i in interface_site_ids:
                raise ValueError("The energy density class has been instanced with a `where' that contains sites at the interface with leads. Calculating energy related quantities on those sites give wrong results.")

    
    def _update_neighbor_lists(self):        
        self._neighbors_i.clear()        
        for hopping in self.where:   
            i = hopping[0]        
            assert(isinstance(i, (int, np.integer)))
            self._neighbors_i.append(np.asarray([k for k in self.syst.graph.out_neighbors(i)], dtype=gint_dtype))

    def _save_static_onsite(self, params=None):
        init_params = params.copy()
        init_params[self.time_name] = self.time_start - 1.0

        for hopping_index in range(self.where_size):
            i = self.where[hopping_index, 0]
            if i not in self._static_onsite.keys():
                self._static_onsite[i] = ta.matrix(self.syst.hamiltonian(i, i, params=init_params), complex)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.embedsignature
    def __call__(self, 
                 psi: Sequence[complex],
                 params: Optional[Dict[str, Union[float, complex]]] = None) -> Union[float, List[float]]:
        r"""Returns the contribution :math:`ρ_i^ε(ψ, t)` of ``psi`` to the expectation value
        of the energy density on each site :math:`i` of the `where` list.

        This method enables calling an instance `A` of this class like a 
        function, with a wave function and the "params" dictionary as parameters :

        >>> A(psi, params)

        For each site :math:`i` it calculates the following sum, with ``psi``
        written as ":math:`ψ`" :

        .. math::
            ρ^ε_i(ψ, t) = \sum_{j} \text{Re} \left [ ψ_i^\dagger ε_{ij} ψ_j \right ]

        Parameters
        ----------
        psi : array of complex values
            The wave function to use for calculation. Must have the same length as the total 
            number of orbitals in the system.

        params : :py:class:`dict`, optional
            Dictionary of parameter names and their values.

        Returns
        -------
        :py:class:`list` [:py:class:`float`] or :py:class:`float`
            List of the values of the contribution of ``psi`` to the energy density on each site 
            :math:`\left [ ρ_i^ε(ψ, t) \right ]_{i \in \text{where}}`, 
            when `sum` :math:`= 0`. Otherwise the sum of these values 
            :math:`\sum_{i \in \text{where}} ρ_i^ε(ψ, t)`.
        """
        cdef gint i, norbs_i, offset_i       
        cdef gint k, norbs_k, offset_k, index_k
        cdef gint[:] neighbors_k_of_i
        cdef int hopping_index     
       
        if psi is None:
            raise TypeError('psi must be an array')        

        psi = np.asarray(psi, dtype=complex)      
        
        tot_norbs = _get_tot_norbs(self.syst)
        
        if psi.shape != (tot_norbs,):
            raise ValueError('psi vector is of incorrect shape')    

        if not self._saved_static_onsite:
            self._saved_static_onsite = 1
            self._save_static_onsite(params=params)       

        cdef complex[:] wf = psi

        result = np.zeros(self.where_size, dtype=float)        
        
        # main loop         
        for hopping_index in range(self.where_size):      

            i = self.where[hopping_index, 0]
            #print('i: ', i)
            _get_orbs(self._site_ranges, i, &offset_i, &norbs_i)          

            psi_i = ta.array(wf[offset_i:(offset_i + norbs_i)])

            #print('ψ_i: ', psi_i)
            psi_i_conj = ta.conjugate(psi_i)

            E_ii = None
          
            if self.operatorType == 0:
                # Use the hamiltonian as onsite                
                E_ii = ta.matrix(self.syst.hamiltonian(i, i, params=params), complex)                     

            elif self.operatorType == 1:
                # We get the hamiltonian at instant 0 as onsite
                E_ii = self._static_onsite[i]           

            else: 
                # Use user provided onsite                
                E_ii = ta.matrix(self.customOnsite(self.syst.sites[i], params=params), complex)

            # Add first the term when k=i, not in the neighbors list. 
            result[hopping_index] += ta.dot(psi_i_conj, ta.dot(E_ii, psi_i)).real

            
            neighbors_k_of_i = self._neighbors_i[hopping_index]

            for index_k in range(neighbors_k_of_i.shape[0]):
                k = neighbors_k_of_i[index_k]
                #print('k: ', k)
                _get_orbs(self._site_ranges, k, &offset_k, &norbs_k)

                psi_k = ta.array(wf[offset_k:(offset_k + norbs_k)], complex)
                #print('ψ_k: ', psi_k)               
                
                # Since i != k, we have E_ik == H_ik
                E_ik = ta.matrix(self.syst.hamiltonian(i, k, params=params), complex)

                result[hopping_index] += ta.dot(psi_i_conj, ta.dot(E_ik, psi_k)).real
                  
                        
        return np.sum(result) if self.sum else result


cdef class LeadHeatCurrent:
    r"""
    An operator that calculates the heat current out of a specific lead, 
    into the central system.
    """
    @cython.embedsignature
    def __init__(self,
                 syst: kwant.system.FiniteSystem,
                 chemical_potential: float,
                 added_lead_sites: Union[Iterable[int], Iterable[kwant.builder.Site]],
                 baseOperator: str ="Kinetic+",
                 customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 dt_hamiltonian: Optional[Callable[[Tuple[kwant.builder.Site, kwant.builder.Site], Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 dt_customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 dt: float =1E-3):

        r"""
        Parameters
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which the heat current will be calculated.        

        chemical_potential: :py:class:`float`
            The chemical potential of the considered lead, save in the 
            `chemical_potential` class attribute.

        added_lead_sites : sequence of :py:class:`int` or `~kwant.builder.Site`
            A set of sites from the lead that have been added to the system 
            by the user. Saved in `added_lead_sites` attribute, this list is 
            used to determine the hoppings used to calculate the heat current, 
            which are at the interface between this list and the central system. 
            The hoppings are stored in the `hoppings` attribute.

        baseOperator : :py:class:`str`
            Can be ``Hamiltonian``, ``Kinetic+`` or ``Custom`` (case unsensitive). 
            It tells the operator which onsite term to use when calculating the 
            energy current.

        customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Overwrites the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, 
            but only when ``baseOperator`` is set to ``Custom``. In practice, its signature 
            should be ``customOnsite(site, params)`` where ``site`` is a `~kwant.builder.Site` 
            instance and ``params`` is a :py:class:`dict` containing the parameters 
            values (including the current time). The method must return a matrix-like
            object (e.g. a `~tinyarray` instance) of the right dimensions at each site 
            (the same as the onsite hamiltonian). It is saved in the `customOnsite` 
            attribute of this class.

        dt_customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            The time derivative of the custom onsite, to be (optionally) given only 
            when ``baseOperator`` is set to ``Custom``. Saved in the `dt_customOnsite` 
            attribute of this class. If specified, its signature 
            should be ``dt_customOnsite(site, params)`` where ``site`` is a
            `~kwant.builder.Site` instance and ``params`` is a :py:class:`dict`
            containing the parameters values (including the current time). 
            The method must return a matrix-like object (e.g. a `tinyarray` 
            instance) of the right dimensions at each site (the same as the 
            onsite hamiltonian). If not specified (thus 
            defaulting to :py:const:`None`), finite difference will be used 
            to estimate the time derivative using the "small" time step `dt`.         

        dt_hamiltonian : :py:const:`None` (default) or (`~kwant.builder.Site`, `~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            The time derivative of the hamiltonian. Saved in the `dt_hamiltonian` 
            attribute of this class. If specified, its signature 
            should be ``dt_hamiltonian(site1, site2, params)`` where ``site1`` and 
            ``site2`` are `~kwant.builder.Site` instances ; ``params`` is a :py:class:`dict` 
            containing the parameters values (including the current time). The method 
            must return a matrix-like object (e.g. a `tinyarray` instance) of the right 
            dimensions for each hopping ``(site1, site2)`` (the same size as the 
            hamiltonian). If not specified (thus defaulting to :py:const:`None`), 
            finite difference will be used to estimate it using the "small" time step `dt`.

        dt : :py:class:`float`, default value: :math:`10^{-3}`
            The time step used to estimate the time derivative of the custom onsite (if ``dt_customOnsite=None``)
            or the Hamiltonian (if ``dt_hamiltonian=None``). 
        
        Notes
        -----
        This class calculates the heat current :math:`I_L^H(t)` flowing out of a given lead :math:`L` to
        the central system :math:`C`:

        .. math::
            I_L^H(t) = \sum_{i \in L, j \in C} I^ε_{ji} - \sum_{i \in L} S^ε_i - μ \sum_{i \in L, j \in C} I^N_{ji}

        where :math:`I^ε_{ji}` is local the energy current flowing from 
        :math:`i` to :math:`j` (calculated by the `~tkwantoperator.EnergyCurrent`
        class); :math:`S^ε_i` is the input power  
        on site :math:`i` (calculated by the `~tkwantoperator.EnergySource` class);
        and :math:`I^{N}_{ji}` is the particle current flowing from site :math:`i` 
        to site :math:`j` (calculated by the `~kwant.operator.Current` class). :math:`μ`
        is the chemical potential of the lead, given by `chemical_potential`.

        Please note that the value of the heat current :math:`I_L^H(t)` depends on the position
        of the interface between the lead and the central system. 
        The user is free to choose this position by adding an arbitrary number of 
        (first) cells of the lead into the central system.

        Moreover, the calculation of :math:`I^ε_{ij}`, :math:`S^ε_i`, and :math:`I^{N}_{ij}` involve 
        the sites :math:`i` and :math:`j`, and also their neighbors (a neighbor of site :math:`i`
        is a site :math:`k \neq i` with :math:`H_{ik} \neq 0`. It is not necessarily a first nearest-neighbor
        but it is directly connected to :math:`i`). Since the wavefunction can be only accessed on sites 
        that lie in the central region, sites :math:`i`, :math:`j`, and their neighbors must belong to the
        central region. A simple way to achieve this is to call `~kwant.builder.Builder.attach_lead` 
        with the additional optional parameter ``add_cells`` set to 
        :math:`2`. This method adds automatically
        two cells from the lead to the central system and returns
        the list of the added sites:

        .. code-block:: python
        
            added_lead_sites = syst.attach_lead(lead, add_cells=2)

        ::

            Before attaching the lead:                  After attaching the lead:
            --------------------------                  -------------------------
                                                                          
                                                                          
            --O----O----O----O     C----C----           --O----O--|--O----O----C----C----
            _________________|     |__________          _______|     |____|     
                     lead            central             lead      added lead      
                                     system                           sites
                                                                     |___________________
                                                                        Central system

        The list of sites `added_lead_sites` is given as an input parameter of `LeadHeatCurrent`
        and it is used to determine the list of hoppings :math:`(j, i)`, saved in the 
        attribute `hoppings`, where :math:`i` belongs to `added_lead_sites` 
        and :math:`j` to the central region for example: ::

                             
                                    i____j
                                    |    |
                  --O----O--|--O----O----C----C----
                   ______|     |____|     
                    lead      added lead      
                                 sites
                               |___________________
                                    Central system
                            

        Note that `added_lead_sites` can be defined by hand by the user without calling `~kwant.builder.Builder.attach_lead`.
        It must contain sites that belonged originally to the lead (in particular the on-site potential
        on these sites is spatially uniform and time-independent).         
        More precisely, for a given position of the :math:`L-C` interface 
        at which the heat current :math:`I_L^H(t)` will be calculated,
        `added_lead_sites` must contain the neighbors that are in :math:`L` of the sites in :math:`C`
        and the neighbors that are in :math:`L` of those former neighbors. 
        It should form one unique closed surface (i.e. a connected graph), and without "holes". 

        Eventually, for each hopping :math:`(j, i)` from `hoppings` a heat current 
        contribution :math:`I_{ji}^H(t) = I^ε_{ji} - S^ε_i - μ I^N_{ji}` is calculated
        and the sum over all heat current contributions is done to deduce :math:`I_L^H(t)`.

        .. rubric:: Attributes

        Attributes
        ----------        

        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which the heat current will be calculated.

        _particle_current_op : `~kwant.operator.Current`
            An instance of Kwant's particle current, used internally to calculate 
            the heat current. The attribute `hoppings` are given to it as a 
            ``where`` at instantiation.

        _energy_source_op : `~tkwantoperator.EnergySource`
            An instance of tKwant's energy source, used internally to calculate 
            the heat current. The attribute `source_sites` are given to it as a 
            ``where``  at instantiation.
        
        _energy_current_op : `~tkwantoperator.EnergyCurrent`
            An instance of tKwant's energy current, used internally to calculate 
            the heat current. The attribute `hoppings` are given to it as a 
            ``where``  at instantiation.

        chemical_potential : double (a Python :py:class:`float` compatible value)
            The chemical potential of the considered lead.
        
        added_lead_sites : 2D array of `~kwant.graph.defs.gint` (a python :py:class:`int` compatible type) of shape :math:`n \times 2`
            List of sites :math:`i` (accessed as ``i = where[n, 0]``) that contains sites
            at the interface with the chosen lead.
        
        hoppings : :py:class:`list` of :py:class:`tuple` of :py:class:`int`  
            Hoppings that are effectively used to calculate the heat current, calculated from `added_lead_sites`,
            as described in the Notes above.
        
        """
        if not isinstance(chemical_potential, (float, np.floating)):
            raise ValueError("The chemical potential must be set and have a real value")

        self.chemical_potential = chemical_potential
        self.syst = syst
        
        if not added_lead_sites:
            raise ValueError("'added_lead_sites' must be set to instantiate class")  
        
        self.added_lead_sites = kwant.operator._normalize_site_where(syst, added_lead_sites)
        self._check_added_lead_sites()

        self.hoppings = list()
        self.source_sites = list()
        self._update_hoppings_and_sites()   

        self._particle_current_op = ParticleCurrent(syst, where=self.hoppings)
        self._energy_current_op = EnergyCurrent(syst, where=self.hoppings, baseOperator=baseOperator, customOnsite=customOnsite)
        self._energy_source_op = EnergySource(syst, where=self.source_sites, baseOperator=baseOperator, dt_hamiltonian=dt_hamiltonian,\
                        dt_customOnsite=dt_customOnsite, customOnsite=customOnsite, dt=dt)
        
    def _check_added_lead_sites(self):
        added_sites = {couple[0] for couple in self.added_lead_sites}
        
        # Check that the user given `added_lead_sites' contain all the sites that are at the interface with the list
        user_given_lead = -1        
        for lead_num, interface in enumerate(self.syst.lead_interfaces):
            if interface[0] in added_sites:
                interface_plus_neighbors = set(interface)
                for site in interface:
                    interface_plus_neighbors.update(self.syst.graph.out_neighbors(site))
                if all([site in added_sites for site in interface_plus_neighbors]):
                    user_given_lead = lead_num
        
        if user_given_lead == -1:
            raise ValueError("The user given `added_lead_sites' variable should contain all the sites at the interface with the lead, and their neighbors.")

        # Check that the sites form a connected component, aka a unique closed surface

        nodes_to_explore = set({self.added_lead_sites[0][0]})
        explored_nodes = set()

        while nodes_to_explore:
            node = nodes_to_explore.pop()
            explored_nodes.add(node)
            for neighbor in self.syst.graph.out_neighbors(node):
                if neighbor in added_sites and neighbor not in explored_nodes:
                    nodes_to_explore.add(neighbor)

        if not explored_nodes >= added_sites:
            raise ValueError("The user given `added_lead_sites' variable should from one unique closed surface (i.e. a connected graph).")        


    def _update_hoppings_and_sites(self):
        self.hoppings.clear()
        self.source_sites.clear()

        interface_site_ids = {site_id for interface in self.syst.lead_interfaces for site_id in interface}
        added_sites = {couple[0] for couple in self.added_lead_sites}
        
        for j in added_sites:                     
            assert(isinstance(j, (int, np.integer)))
            for i in self.syst.graph.out_neighbors(j):
                if i not in added_sites:
                    if i in interface_site_ids or j in interface_site_ids:
                        raise ValueError("The user given `added_lead_sites' variable doesn't contain enough sites: the outgoing hoppings contain lead interface sites, which would give wrong results.")
                    self.hoppings.append((self.syst.sites[i], self.syst.sites[j]))
                    self.source_sites.append(self.syst.sites[j])

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.embedsignature
    def __call__(self, 
                 psi: Sequence[complex],
                 params: Optional[Dict[str, Union[float, complex]]] = None) -> Union[float, List[float]]:
        r"""Returns the contribution :math:`I_L^H(ψ, t)` of ``psi`` to the expectation value
        of the lead's outgoing heat current.

        This method enables calling an instance `A` of this class like a 
        function, with a wave function and the "params" dictionary as parameters :

        >>> A(psi, params)

        It calculates the following sum, with ``psi``
        written as ":math:`ψ`" :

        .. math::
            I_L^H(ψ, t) = \sum_{(j, i) \in \text{hoppings}} I^ε_{ji}(ψ, t) - S^ε_i(ψ, t) - μ I^N_{ji}(ψ, t)

        Parameters
        ----------
        psi : array of complex values
            The wave function to use for calculation. Must have the same length as the total 
            number of orbitals in the system.

        params : :py:class:`dict`, optional
            Dictionary of parameter names and their values.

        Returns
        -------
        :py:class:`float`
            The contribution :math:`I_L^H(ψ, t)` of ``psi`` to the expectation value
            of the lead's outgoing heat current.
        """

        return np.sum(self._energy_current_op(psi, params=params)\
                        - self._energy_source_op(psi, params=params)\
                        - self.chemical_potential * self._particle_current_op(bra=psi, params=params))


cdef class EnergyCurrentDivergence:
    r"""
    An operator that calculates the net energy current leaving a given site
    of the system by summing the outgoing energy currents towards its neighbors.    
    """
    @cython.embedsignature
    def __init__(self,
                 syst: kwant.system.FiniteSystem,
                 where: Union[List[int], List[kwant.builder.Site]],
                 baseOperator: str ="Kinetic+",
                 customOnsite: Optional[Callable[[kwant.builder.Site, Dict[str, Union[float, complex]]], Union[complex, ta.matrix]]]=None,
                 sum: bool =False):
        r"""        
        Parameters
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which the current divergence will be calculated.

        where : :py:class:`list` [:py:class:`int`] or :py:class:`list` [`~kwant.builder.Site`]
            The sites on which the net outgoing energy current will be evaluated. It is 
            saved in a normalized way, with the same order, in the `where` 
            attribute of this class.

        baseOperator : :py:class:`str`
            Can be ``Hamiltonian``, ``Kinetic+`` or ``Custom`` (case unsensitive). 
            It tells the operator which onsite term to use when calculating the 
            energy current. Saved as an integer in `operatorType`

        customOnsite : :py:const:`None` (default) or (`~kwant.builder.Site`, :py:class:`dict`) :math:`\rightarrow` complex matrix-like object
            Overwrites the value of the energy operator :math:`ε` on sites: 
            :math:`\forall i \, \forall t \; ~ ε_{ii}(t) = \text{customOnsite}(i, t)`, 
            but only when ``baseOperator`` is set to ``Custom``. In practice, its signature 
            should be ``customOnsite(site, params)`` where ``site`` is a `~kwant.builder.Site` 
            instance and ``params`` is a :py:class:`dict` containing the parameters 
            values (including the current time). The method must return a matrix-like
            object (e.g. a `~tinyarray` instance) of the right dimensions at each site 
            (the same as the onsite hamiltonian). It is saved in the `customOnsite` 
            attribute of this class.

        sum : :py:class:`bool`
            Whether to sum the divergences over the sites of the `where` list and return a 
            single float, which will be the net current flowing out of the surface(s)
            made by the sites in `where` (using optimised code). Saved as an integer in 
            the `sum` attribute of this class.

        Notes
        -----
        This class calculates the net energy current leaving each site :math:`i`
        given in ``where``, by summing the outgoing energy currents to 
        each of its neighbors :math:`j \neq i`  (with :math:`H_{ji} \neq 0`)

        .. math::
            D_i(t) = \sum_j I^ε_{ji}

        where :math:`I^ε_{ji}` is the energy current flowing from site :math:`i`
        to site :math:`j` (calculated by the class `~tkwantoperator.EnergyCurrent`).

        .. rubric:: Attributes 

        Attributes
        ----------
        syst : `~kwant.system.FiniteSystem`
            The finalized Kwant system on which the current divergence will be calculated.

        where : 2D array of `~kwant.graph.defs.gint` (a python :py:class:`int` compatible type) of shape :math:`n \times 2`
            List of sites :math:`i` (accessed as ``i = where[n, 0]``) on which the energy divergence is calculated at each call.

        where_size : :py:class:`int`
            Number of sites in the `where` list: the size of its first dimension.
        
        sum : :py:class:`int`, :math:`0` (no) or :math:`1` (yes)
            Whether to sum the current divergences over the `where` list and return a 
            single float result.
        
        _energy_current_op : `~tkwantoperator.EnergyCurrent`
            An instance of tKwant's energy current, used internally to calculate 
            the divergences. The attribute `_energy_current_where` is given to it as a 
            ``where`` at instantiation.

        _neighbors_num : :py:class:`list` of :py:class:`int`
            Number of neighbors of each site of the `where` list, with the same order. This 
            attribute is not used if `sum` is set to :math:`1`.
    
        _energy_current_where : :py:class:`list` [(`~kwant.builder.Site`, `~kwant.builder.Site`)]
            List of hoppings on which the energy current will be calculated. Slices of
            this list are then made, with the help of `_neighbors_num`, to calculate the current
            divergence on each site of the `where` list. if `sum` is set to :math:`1`, this list 
            will contain only the outgoing hoppings from the surface(s) made by the sites in
            the `where` list.

        """
        self.syst = syst
        self.where = kwant.operator._normalize_site_where(syst, where)
        self.where_size = self.where.shape[0]
        self.sum = sum
       
        self._update_energy_current_where()

        self._energy_current_op = EnergyCurrent(syst, self._energy_current_where, baseOperator=baseOperator, customOnsite=customOnsite, sum=sum)

    def _update_energy_current_where(self):
        
        self._energy_current_where = list()
        self._neighbors_num = np.zeros(self.where_size, dtype=gint_dtype)

        # Create simple list of the sites of where, to be able to do `j not in where_sites`
        where_sites = {fake_hopping[0] for fake_hopping in self.where}

        cdef int i, j

        for index, fake_hopping in enumerate(self.where):   
            i = fake_hopping[0]
            neighbors = self.syst.graph.out_neighbors(i)
            self._neighbors_num[index] = len(neighbors)

            for j in neighbors:
                if (not self.sum) or (j not in where_sites):
                    self._energy_current_where.append((self.syst.sites[j], self.syst.sites[i]))

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.embedsignature
    def __call__(self, 
                 psi: Sequence[complex],
                 params: Optional[Dict[str, Union[float, complex]]] = None) -> Union[float, List[float]]:
        r"""Returns the contribution :math:`D_i^ε(ψ, t)` of ``psi`` to the expectation value
        of the current divergence on each site of the `where` list.

        This method enables calling an instance `A` of this class like a 
        function, with a wave function and the "params" dictionary as parameters :

        >>> A(psi, params)

        For each site :math:`i` it calculates the following sum, with ``psi``
        written as ":math:`ψ`" :

        .. math::
            D_i^ε(ψ, t) = \sum_j I^ε_{ji}(ψ, t)

        Where :math:`I^ε_{ji}(ψ, t)` is calculated with `~tkwantoperator.EnergyCurrent.__call__`
        from the `~tkwantoperator.EnergyCurrent` class.

        Parameters
        ----------
        psi : array of complex values
            The wave function to use for calculation. Must have the same length as the total 
            number of orbitals in the system.

        params : :py:class:`dict`, optional
            Dictionary of parameter names and their values.

        Returns
        -------
        :py:class:`list` [:py:class:`float`] or :py:class:`float`
            List of the values of the contribution of ``psi`` to the energy current
            divergence on each site of the `where` list 
            :math:`\left [ D_i^ε(ψ, t) \right ]_{i \in \text{where}}`, 
            when `sum` :math:`= 0`. Otherwise the sum of these values 
            :math:`\sum_{i \in \text{where}} D_i^ε(ψ, t)`.
        """

        if self.sum:
            return self._energy_current_op(psi, params=params)
        else:      
            return self._call_per_site_results(psi, params=params)

    @cython.boundscheck(False)
    @cython.wraparound(False)
    def _call_per_site_results(self, psi, params=None):

        cdef double[:] results = np.zeros(self.where_size, dtype=np.double)
        cdef double[:] op_results = self._energy_current_op(psi, params=params)  

        cdef int op_results_index = 0
        cdef int where_site_index = 0  
        cdef int index_to_next_site = self._neighbors_num[where_site_index]    

        for op_results_index in range(op_results.shape[0]):
            if op_results_index >= index_to_next_site:
                where_site_index += 1
                index_to_next_site += self._neighbors_num[where_site_index]

                if where_site_index == self.where_size-1:
                    assert(index_to_next_site == op_results.shape[0])

            results[where_site_index] += op_results[op_results_index]

        return results
