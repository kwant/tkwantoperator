# -*- coding: utf-8 -*-
# Copyright 2020 tkwantoperator authors.

from kwant.graph.defs import gint_dtype
from kwant.graph.defs cimport gint
from kwant.operator cimport Current as ParticleCurrent

cdef class EnergyCurrent:
    cdef public int sum, where_size, operatorType, _saved_static_onsite
    cdef public object syst, customOnsite
    cdef public str time_name
    cdef public float time_start
    cdef public gint[:, :]  where, _site_ranges
    cdef public list _neighbors_i, _neighbors_j # List of lists of the neighbors of each site i and j in of the (j, i) hoppings in the `where` list
    cdef public dict _static_onsite

cdef class EnergySource:
    cdef public int sum, where_size, operatorType, has_dt_hamiltonian, has_dt_customOnsite, _saved_static_onsite
    cdef public object syst, customOnsite, dt_hamiltonian, dt_customOnsite
    cdef public str time_name
    cdef public float time_start
    cdef public double dt
    cdef public gint[:, :]  _site_ranges, where
    cdef public list _neighbors_i # List of lists of the neighbors of each site i in the `where` list
    cdef public dict _static_onsite

cdef class EnergyDensity:
    cdef public int sum, where_size, operatorType, _saved_static_onsite
    cdef public object syst, customOnsite
    cdef public str time_name
    cdef public float time_start
    cdef public gint[:, :]  _site_ranges, where
    cdef public list _neighbors_i # List of lists of the neighbors of each site i in the `where` list
    cdef public dict _static_onsite

cdef class LeadHeatCurrent:
    cdef public object syst
    cdef public ParticleCurrent _particle_current_op
    cdef public EnergySource _energy_source_op
    cdef public EnergyCurrent _energy_current_op
    cdef public double chemical_potential
    cdef public gint[:, :]  added_lead_sites # Sites of the lead that have been added to the system 
    cdef public list hoppings   # Hoppings on wich to calculate the heat current contributions
    cdef public list source_sites # Sites on which to calculate the energy source, it's the head of each hopping of `hoppings`

cdef class EnergyCurrentDivergence:
    cdef public int sum, where_size
    cdef public object syst
    cdef public EnergyCurrent _energy_current_op
    cdef public gint[:, :] where
    cdef public gint[:] _neighbors_num # number of neighbors of each site of the where list
    cdef public list _energy_current_where # The where list of hoppings (j, i) to give to the energy current