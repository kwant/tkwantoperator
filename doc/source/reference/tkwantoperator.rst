Documentation
=============

.. module:: tkwantoperator

tkwantoperator implements additional operators for `tkwant <https://kwant-project.org/extensions/tkwant/>`__ that calculate energy related expectation values. They can be used in the same way as kwant’s operators in the module `~kwant.operator`.

Helper Classes
----------------
.. autosummary::
   :toctree: generated/
   :template: autosummary/class.rst

   EnergySiteValidator

Energy observables
------------------
.. autosummary::
   :toctree: generated/
   :template: autosummary/class.rst

   EnergyCurrent
   EnergySource
   EnergyDensity
   LeadHeatCurrent
   EnergyCurrentDivergence

.. _article-ref:

Reference article
"""""""""""""""""

A. Kara Slimane, P. Reck, G. Fleury, Simulating time-dependent thermoelectric transport in quantum systems
[ `Phys. Rev. B <https://journals.aps.org/prb/abstract/10.1103/PhysRevB.101.235413>`_ | `arXiv <https://arxiv.org/abs/1912.09386>`_ ]