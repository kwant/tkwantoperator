# Example code that uses tkwantoperator

from mpi4py import MPI

import kwant
import tkwant
import tkwantoperator

import numpy as np
import matplotlib.pyplot as plt
import time as timer

# Matplotlib configuration

plt.rcParams['text.usetex'] = True

plt.rcParams['figure.figsize'] = (11.69,8.27)
plt.rcParams['axes.formatter.useoffset'] = True
plt.rcParams['axes.formatter.limits'] = (-2, 2)

plt.rcParams['axes.grid'] = True
plt.rcParams['font.size'] = 20

# Mpi specific function that is helpful when the code is run over several cores
def am_master():
    return MPI.COMM_WORLD.rank == 0

γ  = 1.0          # nearest-neighbor hopping term in the leads
γc = 0.2          # nearest-neighbor hopping term between the central site and the leads

a = 1.0 # lattice constant

Γ = 4 * γc*γc / γ  # Energy scaling unit

ε0 = 0.5 * Γ   # initial dot energy level
Δε = 2.5 * Γ   # change of dot energy level
t0 =  1.5 # time of the heaviside jump

def qdot_potential(site, time, t0, ε0, Δε):
    if time > t0:
        return ε0 + Δε
    else:
        return ε0

lat = kwant.lattice.chain(a, norbs = 1)
builder = kwant.Builder()

# lat(0) is the dot, the rest belongs already formally to the leads
builder[(lat(-1))] = 0
builder[(lat(0))] = qdot_potential
builder[(lat(1))] = 0
builder[lat.neighbors()] = - γc


# Define and attach the leads
lead = kwant.Builder(kwant.TranslationalSymmetry((-a,)))
lead[lat(0)] = 0
lead[lat.neighbors()] = - γ

added_sites_left = builder.attach_lead(lead, add_cells=1)    
# Append lat(-1) to the list, to calculate the heat current between lat(-1) and lat(0)
added_sites_left.append(lat(-1))

added_sites_right = builder.attach_lead(lead.reversed(), add_cells=1)
# Append lat(1) to the list, to calculate the heat current between lat(1) and lat(0)
added_sites_right.append(lat(1))

#show a representation of the 1D chain
if am_master():
    # kwant.plotter.set_engine("matplotlib")
    kwant.plot(builder)

# Create finalized system
syst = builder.finalized()

μL =  0.5 * Γ # Chemical potential in the left lead
μR = -0.5 * Γ # Chemical potential in the right lead

# Occupation, for each lead

TL =  1.0 * Γ # Temperature in the left lead
TR =  0.0 * Γ # Temperature in the right lead

occupation = [None] * len(syst.leads)
occupation[0] = tkwant.manybody.lead_occupation(chemical_potential=μL, temperature=TL)
occupation[1] = tkwant.manybody.lead_occupation(chemical_potential=μR, temperature=TR)

# Maximum simulation time
tmax = 6. / Γ

# Interval between two measurements of the operators
dt = 0.01 / Γ

# Create list of all time steps
times = np.arange(0, tmax, dt)


# Declare operators
particle_current_op = kwant.operator.Current(syst, where=[(lat(0), lat(-1)), (lat(0), lat(1))])
energy_current_op = tkwantoperator.EnergyCurrent(syst, where=[(lat(0), lat(-1)), (lat(0), lat(1))])
energy_source_op  = tkwantoperator.EnergySource(syst, where=[lat(-1),lat(1)])
energy_density_op  = tkwantoperator.EnergyDensity(syst, where=[lat(0)])
lead_heat_current_right_op = tkwantoperator.LeadHeatCurrent(syst, chemical_potential=μR, added_lead_sites=added_sites_right)
lead_heat_current_left_op = tkwantoperator.LeadHeatCurrent(syst, chemical_potential=μL, added_lead_sites=added_sites_left)

# Verify that the automatically calculated hoppings correspond to the ones we expect
# N.B: these lines aren't needed and are here only for illustration

if am_master():
    print(lead_heat_current_left_op.hoppings == [(lat(0), lat(-1))])
    print(lead_heat_current_right_op.hoppings == [(lat(0), lat(1))])

# Declare empty lists that wil be filled with operator expectation values
particle_current = []
energy_current = []
energy_source = []
energy_density = []
heat_current_left = []
heat_current_right = []

# Initialize the solver (to solve t-dep SEQ)
solver = tkwant.manybody.State(syst, tmax, occupation, params={'t0': t0, 'ε0': ε0, 'Δε': Δε}, error_op=energy_density_op)

# Have the system evolve forward in time, calculating the operator over the system
start = timer.perf_counter()
for time in times:
    # evolve scattering states in time
    solver.evolve(time)
    solver.refine_intervals()

    # Evaluate operators at the specific time
    if am_master():
        # Save the expectation values when process is the master
        particle_current.append(solver.evaluate(particle_current_op))
        energy_current.append(solver.evaluate(energy_current_op))
        energy_source.append(solver.evaluate(energy_source_op))
        energy_density.append(solver.evaluate(energy_density_op))
        heat_current_left.append(solver.evaluate(lead_heat_current_left_op))
        heat_current_right.append(solver.evaluate(lead_heat_current_right_op))
    else:
        # processes that aren't the master (rank=0) 
        # do not return the actual expectation value
        # but need to call solver.evaluate() to make 
        # the parallel calculation happen

        solver.evaluate(particle_current_op)
        solver.evaluate(energy_current_op)
        solver.evaluate(energy_source_op)
        solver.evaluate(energy_density_op)
        solver.evaluate(lead_heat_current_left_op)
        solver.evaluate(lead_heat_current_right_op)

    if am_master():
        print("Elapsed time: {:.1f}s,  progress = {:.2f} %".format(timer.perf_counter() - start, time/tmax * 100))

# Only master will plot the results
if am_master():

    # Rescale results
    times = np.array(times) * Γ
    energy_current = np.array(energy_current) / Γ**2
    energy_source = np.array(energy_source) / Γ**2
    heat_current_left = np.array(heat_current_left) / Γ**2
    heat_current_right = np.array(heat_current_right) / Γ**2
    particle_current = np.array(particle_current) / Γ**2

    heat_current_left_2 = energy_current[:, 0] - energy_source[:, 0] - μL * particle_current[:, 0]
    heat_current_right_2 = energy_current[:, 1] - energy_source[:, 1] - μR * particle_current[:, 1]

    energy_density = np.array(energy_density) / Γ

    # Plot
    plt.plot(times, energy_current[:, 0], 'r-', label = "Left incoming energy current $I^\\varepsilon_{0,-1}$")
    plt.plot(times, energy_current[:, 1], 'm-', label = "Right incoming energy current $I^\\varepsilon_{0,1}$")
    plt.plot(times, energy_source[:, 0], 'g-', label = "Left source $S_{-1}$")
    plt.plot(times, energy_source[:, 1], 'y-', label = "Right source $S_{1}$")

    plt.plot(times, heat_current_left, 'c-', label = "Left incoming heat current")
    plt.plot(times, heat_current_right, 'b-', label = "Right incoming heat current")
    plt.plot(times, heat_current_left_2, 'co', label = "$I^\\varepsilon_{0,-1}-S_{-1}-\mu_L I^N_{0,-1}$", markevery=0.025)
    plt.plot(times, heat_current_right_2, 'bo', label = "$I^\\varepsilon_{0,1}-S_{1}-\mu_R I^N_{0,1}$", markevery=0.025)

    plt.xlabel('time $[\\hbar / \\Gamma]$')
    plt.ylabel('Energy flux $[\\Gamma^2 / \\hbar]$')
    plt.legend()
    plt.show()

    plt.plot(times, energy_density, label = "Energy density $\\varepsilon_0$")
    plt.xlabel('time $[\\hbar / \\Gamma]$')
    plt.ylabel('Energy density $[\\Gamma]$')
    plt.legend()
    plt.show()