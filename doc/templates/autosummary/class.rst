{% extends "!autosummary/class.rst" %}

{% block methods %}
{% if members %}
   .. rubric:: Methods
   {% for item in methods %}
      {% if not item.startswith('_') %}
   .. automethod:: {{ name }}.{{ item }}
      {% endif %}
      {% if '__call__' in members%}
   .. automethod:: {{ name }}.__call__
      {% endif %}
   {%- endfor %}
{% endif %}
{% endblock %}

{% block attributes %}

{% endblock %}
